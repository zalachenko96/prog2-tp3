package punto1.cliente;

public class Vacuna{

    private String fecha;
    private String enfermedad;
    private String tipoVacuna;

    public Vacuna(String fecha, String enfermedad, String tipoVacuna)
    {
        this.fecha=fecha;
        this.enfermedad=enfermedad;
        this.tipoVacuna=tipoVacuna;
    }

    public String getFecha()
    {
        return fecha;
    }

    public void setFecha(String fecha)
    {
        this.fecha=fecha;
    }

    public String getEnfermedad()
    {
        return enfermedad;
    }

    public void setEnfermedad(String enfermedad)
    {
        this.enfermedad=enfermedad;
    }

    public String getTipoVacuna()
    {
        return tipoVacuna;
    }

    public void setTipoVacuna(String tipoVacuna)
    {
        this.tipoVacuna=tipoVacuna;
    }
}