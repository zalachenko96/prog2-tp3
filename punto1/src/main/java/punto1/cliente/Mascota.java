package punto1.cliente;

public class Mascota {

    private String cod;
    private String alias;
    private String especie;
    private String raza;
    private String colorPelo;
    private String fechaNac;
    private Integer nroVisita;
    private float pesoPromedio;
    private float ultimoPeso;
    private String historiaClinica;

    public Mascota(String cod, String alias, String especie, String raza, String colorPelo, String fechaNac, Integer nroVisita, float pesoPromedio, float ultimoPeso, String historiaClinica){
        this.cod=cod;
        this.alias=alias;
        this.especie=especie;
        this.raza=raza;
        this.colorPelo=colorPelo;
        this.fechaNac=fechaNac;
        this.nroVisita=nroVisita;
        this.pesoPromedio=pesoPromedio;
        this.ultimoPeso=ultimoPeso;
        this.historiaClinica=historiaClinica;
    }

    public String getCod(){
        return cod;
    }

    public void setCod(String cod){
        this.cod=cod;
    }

    public String getAlias(){
        return alias;
    }

    public void setAlias(String alias){
        this.alias=alias;
    }

    public String getEspecie(){
        return especie;
    }

    public void setEspecie(String especie){
        this.especie=especie;
    }

    public String getRaza(){
        return raza;
    }

    public void setRaza(String raza){
        this.raza=raza;
    }

    public String getColorPelo(){
        return colorPelo;
    }

    public void setColorPelo(String colorPelo){
        this.colorPelo=colorPelo;
    }

    public String getFechaNac(){
        return fechaNac;
    }

    public void setFechaNac(String fechaNac){
        this.fechaNac=fechaNac;
    }

    public Integer getNroVisita(){
        return nroVisita;
    }

    public void setNroVisita(Integer nroVisita){
        this.nroVisita=nroVisita;
    }

    public float getPesoPromedio(){
        return pesoPromedio;
    }

    public void setPesoPromedio(float pesoPromedio){
        this.pesoPromedio=pesoPromedio;
    }

    public float getUltimoPeso(){
        return ultimoPeso;
    }

    public void setUltimoPeso(float ultimoPeso){
        this.ultimoPeso=ultimoPeso;
    }

    public String getHistoria(){
        return historiaClinica;
    }

    public void setHistoria(String historiaClinica){
        this.historiaClinica=historiaClinica;
    }
}