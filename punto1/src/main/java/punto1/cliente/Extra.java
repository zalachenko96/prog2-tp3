package punto1.cliente;

public abstract class Extra{
    
    abstract public double calcularDescuento();

    abstract public void notificarTratamiento();

}