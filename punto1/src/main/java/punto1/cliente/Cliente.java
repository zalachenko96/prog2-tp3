package punto1.cliente;
import java.util.Scanner;

import java.util.ArrayList;

public class Cliente extends Extra{
        private String codCliente;
        private String apeRespon;
        private Long numBanco;
        private String dirCliente;
        private Long telCliente;
        private ArrayList<Persona> persona= new ArrayList <Persona>();
        private ArrayList<Mascota> mascota = new ArrayList<Mascota>();
        private double descuento;
        private double precioTratamiento;
        private String enviarTratamiento;

        public Cliente(String codCliente, String apeRespon, Long numBanco, String dirCliente, Long telCliente, int descuento, double precioTratamiento){
            this.codCliente= codCliente;
            this.apeRespon= apeRespon;
            this.numBanco=numBanco;
            this.dirCliente=dirCliente;
            this.telCliente=telCliente;
            this.descuento=descuento;
            this.precioTratamiento=precioTratamiento;
        }

        public ArrayList<Persona> getPersona(){
            return persona;
        }

        public void setPersona(Persona p){
            persona.add(p);
        }

        public ArrayList<Mascota> getMascota(){
            return mascota;
        }

        public void setMascota(Mascota m){
            mascota.add(m);
        }

        public String getCodCliente(){
            return codCliente;
        }

        public void setCodCliente(String codCliente){
            this.codCliente=codCliente;
        }

        public String getApeRespon(){
            return apeRespon;
        }

        public void setApeRespon(String apeRespon){
            this.apeRespon=apeRespon;
        }

        public Long getNumBanco(){
            return numBanco;
        }

        public void setNumBanco(Long numBanco){
            this.numBanco=numBanco;
        }

        public String getDirCliente(){
            return dirCliente;
        }

        public void setDirCliente(String dirCliente){
            this.dirCliente=dirCliente;
        }

        public Long getTelCliente(){
            return telCliente;
        }

        public void setTelCliente(Long telCliente){
            this.telCliente=telCliente;
        }

        public double getDescuento(){
            return descuento;
        }

        public void setDescuento(double descuento){
            this.descuento=descuento;
        }

        public void verificarCuenta(Long numBanco){
            Integer digitos = Long.toString(numBanco).length();
            if (digitos<8){
                System.out.printf("El numero ingresado es demasiado pequeño");
            }
            else{
                System.out.printf("La cuenta bancaria se ha agregado correctamente");
            }
        }

        

         @Override
            public double calcularDescuento(){
                if (mascota.size()<=2){
                    descuento=precioTratamiento*0.5;
                }else{
                    descuento=precioTratamiento*0.1;
                }
                return 0;
            }

        @Override
            public void notificarTratamiento(){
                System.out.println("Desea ser notificado via celular o email?");
                Scanner eleccion= new Scanner(System.in);
                String enviarTratamiento=eleccion.nextLine();
                if(enviarTratamiento=="email"){
                    System.out.println(Mascota.getHistoria());
                }else{
                    System.out.println(Mascota.getHistoria());
                }
            }

}