package punto1.cliente;

public class Persona{

    private String nombrePersona;
    private long dni;

    public Persona(String nombrePersona, long dni)
    {
        this.nombrePersona=nombrePersona;
        this.dni=dni;
    }
    
    public String getNombrePersona()
    {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona)
    {
        this.nombrePersona=nombrePersona;
    }

    public long getDni()
    {
        return dni;
    }

    public void setDni(long dni)
    {
        this.dni=dni;
    }
}