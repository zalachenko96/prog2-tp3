package punto2.empleado;

public class Contrato {

    private float sueldo;
    private float total;
    private String fecha;
    

    public Contrato(float sueldo, float total, String fecha)
        {
            this.sueldo=sueldo;
            this.total=total;
            this.fecha=fecha;
        }

    public float getSueldo()
        {
            return sueldo;
        }

    public void setSueldo(float sueldo)
        {
            this.sueldo=sueldo;
        }

    public float getTotal()
        {
            return total;
        }

    public void setTotal(float total)
        {
            this.total=total;
        }

    public String getFecha()
        {
            return fecha;
        }

    public void setFecha(String fecha)
        {
            this.fecha=fecha;
        }
}