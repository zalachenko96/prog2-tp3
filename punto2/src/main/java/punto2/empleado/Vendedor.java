package punto2.empleado;

public class Vendedor extends Empleado{
    private float adicional;
    private Integer contratos;
    private float porcentaje;

    public Vendedor(String nombre, String apellido, String documento, String domicilio, float adicional, Integer contratos, float porcentaje){
        super(nombre, apellido, documento, domicilio);
        this.adicional=adicional;
        this.contratos=contratos;
        this.porcentaje=porcentaje;
    }
    
    public float getAdicional(){
        return adicional;
    }

    public void setAdicional(float adicional){
        this.adicional=adicional;
    }

    public Integer getContratos(){
        return contratos;
    }

    public void setContratos(Integer contratos){
        this.contratos=contratos;
    }

    public float getPorcentaje(float porcentaje){
        return porcentaje;
    }

    public void setPorcentaje(float porcentaje){
        this.porcentaje=porcentaje;
    }
}