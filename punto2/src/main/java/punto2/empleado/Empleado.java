package punto2.empleado;

public class Empleado{

    private String nombre;
    private String apellido;
    private String documento;
    private String domicilio;
    private Contrato contrato;

    public Empleado(String nombre, String apellido, String documento, String domicilio){
        this.nombre=nombre;
        this.apellido=apellido;
        this.documento=documento;
        this.domicilio=domicilio;
    }

    public Empleado(String nombre2, String apellido2, String documento2, String domicilio2, Contrato contrato2) {
	}

	public String getNombre(){
        return nombre;
    }

    public void setNombre(String nombre){
        this.nombre=nombre;
    }

    public String getApellido(){
        return apellido;
    }

    public void setApellido(String apellido){
        this.apellido=apellido;
    }

    public String getDocumento(){
        return documento;
    }

    public void setDocumento(String documento){
        this.documento=documento;
    }

    public String getDomicilio(){
        return domicilio;
    }

    public void setDomicilio(String domicilio){
        this.domicilio=domicilio;
    }




}