package punto2.empleado;

public class Venta{
    private String fecha;
    private String descripcion;
    private float total;

    public Venta(String fecha, String descripcion, float total){
        this.fecha=fecha;
        this.descripcion=descripcion;
        this.total=total;
    }

    public String getFecha(){
        return fecha;
    }

    public void setFecha(String fecha){
        this.fecha=fecha;
    }

    public String getDescripcion(){
        return descripcion;
    }

    public void setDescripcion(String descripcion){
        this.descripcion=descripcion;
    }

    public float getTotal(){
        return total;
    }

    public void setTotal(float total){
        this.total=total;
    }
}