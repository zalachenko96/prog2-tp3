package punto2.empleado;

public class Encargado extends Empleado{
    private float adicional;
    private float porcentaje;

    public Encargado(float adicional, float porcentaje){
        this.adicional=adicional;
        this.porcentaje=porcentaje;
    }

    public float getAdicional(){
        return adicional;
    }

    public void setAdicional(float adicional){
        this.adicional=adicional;
    }

    public float getPorcentaje(){
        return porcentaje;
    }

    public void setPorcentaje(float porcentaje){
        this.porcentaje=porcentaje;
    }

    
}