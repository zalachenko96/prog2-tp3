package punto2.empleado;

public class Limpiador extends Empleado{

    private float adicional;
    private Integer horaextra;
    private float porcentaje;

    public Limpiador(String nombre, String apellido, String documento, String domicilio,float adicional,Integer horaextra,float porcentaje) {
        super(nombre, apellido, documento, domicilio);
        this.adicional = adicional;
        this.horaextra = horaextra;
        this.porcentaje = porcentaje;
    }

    public float getAdicional() {
        return adicional;
    }

    public void setAdicional(float adicional) {
        this.adicional = adicional;
    }

    public Integer getHoraextra() {
        return horaextra;
    }

    public void setHoraextra(Integer horaextra) {
        this.horaextra = horaextra;
    }

    public float getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(float porcentaje) {
        this.porcentaje = porcentaje;
    }


    }